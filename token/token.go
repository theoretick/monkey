package token

const (
	// ILLEGAL represents an illegal character we dont know about
	ILLEGAL = "ILLEGAL"
	// EOF represents the end of a given file when parsing can stop
	EOF = "EOF"

	// Identifiers + literals

	// IDENT represents an identifier
	IDENT = "IDENT"
	// INT represents an integer literal
	INT = "INT"

	// Operators

	// ASSIGN represents an assignment operator
	ASSIGN = "="
	// PLUS represents a mathematical summation operator
	PLUS = "+"

	// MINUS represents an subtraction
	MINUS = "-"
	// BANG represents a negation
	BANG = "!"
	// ASTERISK represents a multiplication
	ASTERISK = "*"
	// SLASH represents a division
	SLASH = "/"
	// LT represents a less than relationship between tokens
	LT = "<"
	// GT represents a greater than relationship between tokens
	GT = ">"

	// Delimiters

	// COMMA represents a single comma delimiter
	COMMA = ","
	// SEMICOLON represents a single semicolon delimiter
	SEMICOLON = ";"
	// LPAREN represents a single paren delimiter
	LPAREN = "("
	// RPAREN represents a single paren delimiter
	RPAREN = ")"
	// LBRACE represents a single brace delimiter
	LBRACE = "{"
	// RBRACE represents a single brace delimiter
	RBRACE = "}"

	// Keywords

	// FUNCTION represents a `function` token
	FUNCTION = "FUNCTION"

	// LET represents a `let` token
	LET = "LET"

	// TRUE represents a true boolean value
	TRUE = "true"

	// FALSE represents a false boolean value
	FALSE = "false"

	// IF represents a conditional branching evaluation
	IF = "IF"

	// ELSE represents a final conditional branch
	ELSE = "ELSE"

	// RETURN represents a return statement
	RETURN = "RETURN"

	// EQ represents an equal equality
	EQ = "=="

	// NOT_EQ represents an inequal equality
	NOT_EQ = "!="
)

var keywords = map[string]TokenType{
	"fn":     FUNCTION,
	"let":    LET,
	"true":   TRUE,
	"false":  FALSE,
	"if":     IF,
	"else":   ELSE,
	"return": RETURN,
}

// TokenType represents the type of a specific token
type TokenType string

// Token represents a single token as generated by our lexer
type Token struct {
	Type    TokenType
	Literal string
}

// LookupIdent returns either a keyword type or IDENT type to bisect reserved words
func LookupIdent(ident string) TokenType {
	if tok, ok := keywords[ident]; ok {
		return tok
	}

	return IDENT
}
